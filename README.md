1) php artisan migrate
2) php artisan db:seed --class=TenantSeeder
3) php artisan db:seed --class=StoreSeeder
4) php artisan db:seed --class=ChargingStationSeeder
5) php artisan db:seed --class=OpenHoursSeeder
6) php artisan db:seed --class=ExceptionTimeSeeder
