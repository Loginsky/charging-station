<?php

use Illuminate\Database\Seeder;

class TenantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aTenants = ["Tenant 1", "Tenant 2", "Tenant 3"];

        foreach($aTenants as $name) {
            $tenant = new \App\Tenant();
            $tenant->name = $name;
            $tenant->save();
        }
    }
}
