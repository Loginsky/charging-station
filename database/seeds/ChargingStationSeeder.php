<?php

use Illuminate\Database\Seeder;

class ChargingStationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aStations = [
            // [store_id => station->number]
            [1 => 2],
            [1 => 3],
            [1 => 33],
            [2 => 4],
            [2 => 55],
            [2 => 5],
            [3 => 6],
            [3 => 7],
            [3 => 99],
        ];

        foreach($aStations as $aStation) {
            $station = new \App\ChargingStation();
            $station->store_id = key($aStation);
            $station->station_number = key(array_flip($aStation));
            $station->save();
        }
    }
}
