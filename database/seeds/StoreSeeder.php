<?php

use Illuminate\Database\Seeder;

class StoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aStores = [
            [1 => 'Madagaskar'],
            [1 => 'Blue Sky'],
            [2 => 'Get Your Dream'],
            [2 => 'Future Charging'],
            [3 => 'Summer Shop'],
            [3 => 'Tesco Charging']
        ];

        foreach($aStores as $aStore) {
            $store = new \App\Store();
            $store->tenant_id = key($aStore);
            $store->name = key(array_flip($aStore));
            $store->save();
        }
    }
}
