<?php

use Illuminate\Database\Seeder;

class ExceptionTimeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // for store_id = 3
        $exception = new \App\ExceptionTime();
        $exception->exceptionable_id = 3;
        $exception->exceptionable_type = 'App\Store';
        $exception->description = 'Closed for some reason and belongs to Store=33';
        $exception->start_datetime = '2020-07-01 09:00:00';
        $exception->end_datetime = '2020-08-15 21:00:00';
        $exception->save();

        // for station_id = 9
        $exception = new \App\ExceptionTime();
        $exception->exceptionable_id = 9;
        $exception->exceptionable_type = 'App\ChargingStation';
        $exception->description = 'Closed for some reason 2 and belongs to Station=99';
        $exception->start_datetime = '2020-07-21 18:00:00';
        $exception->end_datetime = '2020-08-20 21:00:00';
        $exception->save();
    }
}
