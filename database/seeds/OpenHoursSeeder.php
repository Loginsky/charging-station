<?php

use Illuminate\Database\Seeder;
use App\ChargingStation;
use App\Store;
use App\Tenant;

class OpenHoursSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Every `week_day` with different open-time has new record */
        $aStoreStationUserWeekdayOpeningHoursAll = [
            // [ ownable_id => [ ownable_type => [ user_type => [ week_day => "8:00-12:00" ] ] ] ]

            // CUSTOMERS (station_number = 55)--------------------------------|
            [3 => ['App\ChargingStation' => [1 => [1 => "8:00-12:00" ]]]],// Mondays
            [3 => ['App\ChargingStation' => [1 => [1 => "13:00-17:30" ]]]],// Mondays
            [3 => ['App\ChargingStation' => [1 => [2 => "8:00-12:00" ]]]],// Tuesdays
            [3 => ['App\ChargingStation' => [1 => [2 => "13:00-17:30" ]]]],// Tuesdays
            [3 => ['App\ChargingStation' => [1 => [4 => "8:00-12:00" ]]]],// Thursdays
            [3 => ['App\ChargingStation' => [1 => [4 => "13:00-17:30" ]]]],// Thursdays
            [3 => ['App\ChargingStation' => [1 => [3 => "8:00-13:00" ]]]],// Wednesdays
            [3 => ['App\ChargingStation' => [1 => [5 => "8:00-12:00" ]]]],// Fridays
            [3 => ['App\ChargingStation' => [1 => [5 => "13:00-20:00" ]]]],// Fridays

            // EMPLOYEES (station_number = 55)
            [3 => ['App\ChargingStation' => [2 => [1 => "6:30-19:00" ]]]],// Mondays
            [3 => ['App\ChargingStation' => [2 => [2 => "6:30-19:00" ]]]],// Tuesdays
            [3 => ['App\ChargingStation' => [2 => [4 => "6:30-19:00" ]]]],// Thursdays
            [3 => ['App\ChargingStation' => [2 => [3 => "6:30-14:30" ]]]],// Wednesdays
            [3 => ['App\ChargingStation' => [2 => [5 => "6:30-21:00" ]]]],// Fridays
            [3 => ['App\ChargingStation' => [2 => [5 => "9:00-14:30" ]]]],// Saturdays

            // CUSTOMERS (station_number = 99) ------------------------------ |
            // we will use some Exceptions here
            [9 => ['App\ChargingStation' => [1 => [1 => "8:00-12:00" ]]]],// Mondays
            [9 => ['App\ChargingStation' => [1 => [1 => "13:00-17:30" ]]]],// Mondays
            [9 => ['App\ChargingStation' => [1 => [2 => "8:00-12:00" ]]]],// Tuesdays
            [9 => ['App\ChargingStation' => [1 => [2 => "13:00-17:30" ]]]],// Tuesdays
            [9 => ['App\ChargingStation' => [1 => [4 => "8:00-12:00" ]]]],// Thursdays
            [9 => ['App\ChargingStation' => [1 => [4 => "13:00-17:30" ]]]],// Thursdays
            [9 => ['App\ChargingStation' => [1 => [3 => "8:00-13:00" ]]]],// Wednesdays
            [9 => ['App\ChargingStation' => [1 => [5 => "8:00-12:00" ]]]],// Fridays
            [9 => ['App\ChargingStation' => [1 => [5 => "13:00-20:00" ]]]],// Fridays

            // EMPLOYEES (station_number = 99)
            [9 => ['App\ChargingStation' => [2 => [1 => "6:30-19:00" ]]]],// Mondays
            [9 => ['App\ChargingStation' => [2 => [2 => "6:30-19:00" ]]]],// Tuesdays
            [9 => ['App\ChargingStation' => [2 => [4 => "6:30-19:00" ]]]],// Thursdays
            [9 => ['App\ChargingStation' => [2 => [3 => "6:30-14:30" ]]]],// Wednesdays
            [9 => ['App\ChargingStation' => [2 => [5 => "6:30-21:00" ]]]],// Fridays
            [9 => ['App\ChargingStation' => [2 => [5 => "9:00-14:30" ]]]],// Saturdays
        ];


        // Seed data ( for two stations - 55 and 99 )
        /* Every single `week_day` with different open-closed-time - has new record */
        foreach($aStoreStationUserWeekdayOpeningHoursAll as $k => $aStoreStationUserWeekdayOpeningHours) {
            foreach($aStoreStationUserWeekdayOpeningHours as $store_id => $aStationEntity) {
                foreach ($aStationEntity as $strEntity => $aUserType) {
                    foreach ($aUserType as $user_type => $aWeekDays) {
                        foreach ($aWeekDays as $week_day => $startEndHours) {
                            $openHours = new \App\OpenHours();
                            $openHours->ownable_id = $store_id;
                            $openHours->ownable_type = $strEntity;
                            $openHours->user_type = $user_type;
                            $openHours->week_day = $week_day;
                            $aStartEnd = explode('-', $startEndHours);
                            $openHours->start_hour = $aStartEnd[0];
                            $openHours->end_hour = $aStartEnd[1];
                            $openHours->save();
                        }
                    }
                }
            }
        }// end foreach


    }
}
