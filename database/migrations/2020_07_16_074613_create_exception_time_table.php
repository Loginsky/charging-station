<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExceptionTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exception_time', function (Blueprint $table) {
            $table->increments('id');

            // polymorphic relations
            $table->integer('exceptionable_id', 0, 1)->index();
            $table->string('exceptionable_type')->index();

            $table->dateTime('start_datetime');
            $table->dateTime('end_datetime');

            $table->string('description');
            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exception_time');
    }
}
