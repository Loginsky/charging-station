<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpenHoursTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('open_hours', function (Blueprint $table) {
            $table->id();

            // polymorphic relations
            $table->bigInteger('ownable_id')->index();
            $table->string('ownable_type')->index();

            // OpenHours::USER_CUSTOMER=1, 2, 3
            $table->tinyInteger('user_type');

            // OpenHours::WEEKDAY_MONDAY=1, 2, 3, 4, 5, 6, 7
            $table->tinyInteger('week_day')->index();// Defines new record

            $table->time('start_hour');
            $table->time('end_hour');

            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('open_hours');
    }
}
