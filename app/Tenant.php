<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Tenant extends Model
{
    use Notifiable;

    protected $table = 'tenant';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
    ];

    /**
     * Get all of the tenants's `open_hours`.
     */
    public function openHours()
    {
        return $this->morphMany('App\OpenHours', 'ownable');
    }

    /**
     * Get all of the tenant's `exception_time`.
     */
    public function exceptionTime()
    {
        return $this->morphMany('App\ExceptionTime', 'exceptionable');
    }

    public function store()
    {
        return $this->hasMany('App\Store');
    }
}
