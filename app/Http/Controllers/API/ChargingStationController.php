<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\ChargingStation;
use App\Store;
use App\Tenant;
use App\ExceptionTime;
use App\OpenHours;

/**
 * Class ChargingStationController
 * @package App\Http\Controllers\API
 */
class ChargingStationController extends Controller
{

    /**
     * @param Request $request
     * @param int $stationId  ChargingStation id
     * ( To identify ChargingStation - where user want to charge at specific time,
     *      I used - "$stationId" in url params and "url?timestamp=.." or "url?datetime=.." in GET params )
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request, int $stationId)
    {
        $requestToDatetime = "";
        $requestToTimestamp = "";

        /* validate "datetime" passed via url parameter for testing */
        $validator = Validator::make($request->all(), [
            'datetime' => 'date_format:Y-m-d H:i:s'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        if($request->has('datetime')) {
            $requestToDatetime = $request->get('datetime');
            $requestToTimestamp = Carbon::parse($requestToDatetime)->timestamp;
        }
        /* validate "timestamp" (not required yet) */
        if($request->has('timestamp') && !is_integer($request->get('timestamp')) && strlen($request->get('timestamp'))!=10) {
            return response()->json(['error' => "Wrong timestamp."], 400);
        }
        if($request->has('timestamp')) {
            $requestToDatetime = Carbon::createFromTimestamp($request->get('timestamp'))->toDateTimeString();
            $requestToTimestamp = (int) $request->get('timestamp');
        }


        /* Find - ChargingStation (with it's Store) by "$stationId" */
        $chargingStation = ChargingStation::with('store')->find($stationId);
        if(!$chargingStation) {
            return response()->json(['error' => "ChargingStation not found."], 400);
        }
        /* Using `user_type` manually cos it has to come from Auth request headers somewhere */
        $aStationOpenHours = $chargingStation->openHours->where('user_type', '=', OpenHours::USER_CUSTOMER)->toArray();

        /* Compute next OpenHour from $requestToDatetime */
        $computeOpenHours = $chargingStation->computeOpenHours($aStationOpenHours, $requestToDatetime);

        if($computeOpenHours === true) {
            // ChargingStation by $stationId - open 24/7/365
            $requestToDatetime = Carbon::now()->toDateTimeString();
            $requestToTimestamp = Carbon::now()->timestamp;
        } else {
            /* Initialize next OpenClosed hours */
            $chargingStation->initializeOpenHours(array_values($computeOpenHours));
        }


        /* Response */
        if(request()->segment(3) === 'next-time-open') {
            return response()->json([
                'request_timestamp' => $requestToTimestamp,
                'request_datetime' => $requestToDatetime,
                'next_open_timestamp' => ($computeOpenHours === true) ? $requestToTimestamp : ChargingStation::$nextStartTimestamp,
                'next_closed_timestamp' => ($computeOpenHours === true) ? '' : ChargingStation::$nextEndTimestamp,
                'next_open_datetime' => ($computeOpenHours === true) ? $requestToTimestamp : ChargingStation::$nextStartDatetime,
                'next_closed_datetime' => ($computeOpenHours === true) ? $requestToTimestamp : ChargingStation::$nextEndDatetime,
                'next_open_closed' => (is_array($computeOpenHours)) ? array_values($computeOpenHours) : 'open 24/7/365',
            ], 200);
        }

        /* Initialize - is_currently_open */
        if($computeOpenHours !== true) {
            $arr = array_values($computeOpenHours);
            ChargingStation::$isCurrentlyOpen = (isset($arr[0]) && ChargingStation::$isCurrentlyOpen == $arr[0]);
        } else {
            ChargingStation::$isCurrentlyOpen = true;
        }

        /* Response - is_currently_open */
        return response()->json([
            'is_currently_open' => ChargingStation::$isCurrentlyOpen
        ], 200);

    }

}
