<?php

namespace App;

use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class ChargingStation extends Model
{
    use Notifiable;

    protected $table = 'charging_station';

    public static $nextStartTimestamp = '';
    public static $nextEndTimestamp = '';
    public static $nextStartDatetime = '';
    public static $nextEndDatetime = '';
    public static $isCurrentlyOpen = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'store_id', 'station_number', 'user_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'station_number' => 'integer',
        'user_type' => 'integer',
    ];

    /**
     * Get all of the station's `open_hours`.
     */
    public function openHours()
    {
        return $this->morphMany('App\OpenHours', 'ownable');
    }

    /**
     * Get all of the charging_station's `exception_time`.
     */
    public function exceptionTime()
    {
        return $this->morphMany('App\ExceptionTime', 'exceptionable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function store()
    {
        return $this->belongsTo('App\Store', 'store_id', 'id');
    }

    /**
     * @param array $openHours
     * @param datetime $requestDatetime
     *
     * @return bool|array
     */
    public function computeOpenHours(Array $openHours, $requestDatetime)
    {
        // Open 24/7/365
        if(!$openHours)
            return true;

        $aNextOpenHours = [];

        /* 1. Prepare For Sorting */
        foreach($openHours as $k => $arrOpenHour) {
            $weekYmd = (new Carbon())->weekday($arrOpenHour['week_day'])->format('Y-m-d');

            // start date
            $startDate = $weekYmd.' '.$arrOpenHour['start_hour'];
            $openHours[$k]['startDate'] = $startDate;
            // end date
            $endDate = $weekYmd.' '.$arrOpenHour['end_hour'];
            $openHours[$k]['endDate'] = $endDate;

            // set new DateTime for sorting
            $openHours[$k]['startTimestamp'] = Carbon::parse($startDate)->timestamp;
            $openHours[$k]['endTimestamp'] = Carbon::parse($endDate)->timestamp;
        }

        /* 2. Sort by Timestamp */
        usort($openHours, function($a, $b) {
                return ($a['startTimestamp'] < $b['startTimestamp']) ? -1 : 1;
        });

        /* 3. set $aNextOpenHours */
        foreach($openHours as $k => $arrOpenHour) {
            // also if station is available for next 30 min - it will be in "next_start_timestamp" in Response
            if(
                $arrOpenHour['startTimestamp'] > Carbon::parse($requestDatetime)->timestamp ||
                $arrOpenHour['endTimestamp'] > Carbon::parse($requestDatetime)->addMinutes(30)->timestamp
            )
            {
                $aNextOpenHours[] = $arrOpenHour['startDate'] .' - '. $arrOpenHour['endDate'];
                if(!$k) {
                    self::$isCurrentlyOpen = $arrOpenHour['startDate'] .' - '. $arrOpenHour['endDate'];
                }
            }
        }


        $nextWeek = 1;
        while(!$aNextOpenHours) {

            /* 4. If no OpenHours this week anymore, get $aNextWeekOpenHours */
            $aNextWeekOpenHours = $this->nextWeekOpenHours($openHours, $nextWeek);
            foreach ($aNextWeekOpenHours as $arrOpenHour) {
                if(
                    $arrOpenHour['startTimestamp'] > Carbon::parse($requestDatetime)->timestamp ||
                    $arrOpenHour['endTimestamp'] > Carbon::parse($requestDatetime)->timestamp
                )
                {
                    $aNextOpenHours[] = $arrOpenHour['startDate'] . ' - ' . $arrOpenHour['endDate'];
                    if(!$k) {
                        self::$isCurrentlyOpen = $arrOpenHour['startDate'] .' - '. $arrOpenHour['endDate'];
                    }
                }
            }

            /* 5. Compare OpenHours with Exceptions */
            $aNextOpenHours = $this->computeOpenHoursInExceptions($aNextOpenHours);

            $nextWeek++;
        }

        return $aNextOpenHours;
    }


    /**
     * Get next week OpenHours
     * @param $openHours
     * @param int $numOfWeeks
     * @return mixed
     */
    private function nextWeekOpenHours($openHours, int $numOfWeeks = 1) {
        // $openHours already sorted by timestamp here
        foreach($openHours as $k => $arrOpenHour) {
            $weekYmd = (new Carbon())->weekday($arrOpenHour['week_day'])->addWeeks($numOfWeeks)->format('Y-m-d');

            // start date
            $startDate = $weekYmd.' '.$arrOpenHour['start_hour'];
            $openHours[$k]['startDate'] = $startDate;
            // end date
            $endDate = $weekYmd.' '.$arrOpenHour['end_hour'];
            $openHours[$k]['endDate'] = $endDate;

            // set new DateTime for sorting
            $openHours[$k]['startTimestamp'] = Carbon::parse($startDate)->timestamp;
            $openHours[$k]['endTimestamp'] = Carbon::parse($endDate)->timestamp;
        }
        return $openHours;
    }


    /**
     * @param $aNextOpenHours
     * @return array
     */
    private function computeOpenHoursInExceptions($aNextOpenHours)
    {

        /* Tenant exceptions */
        $aTenantExceptions = $this->store->tenant->exceptionTime->toArray();
        foreach($aTenantExceptions as $tenantException) {
            $startExc = Carbon::parse($tenantException['start_datetime']);
            $endExc = Carbon::parse($tenantException['end_datetime']);
            foreach($aNextOpenHours as $k => $fromTo) {
                list($carbonFrom, $carbonTo) = $this->datesToCarbons($fromTo);
                if($carbonFrom->between($startExc, $endExc) && $carbonTo->between($startExc, $endExc)) {
                    unset($aNextOpenHours[$k]);
                }
            }
        }


        /* Store exceptions */
        $aStoreExceptions = $this->store->exceptionTime->toArray();
        foreach($aStoreExceptions as $storeException) {
            $startExc = Carbon::parse($storeException['start_datetime']);
            $endExc = Carbon::parse($storeException['end_datetime']);
            foreach($aNextOpenHours as $k => $fromTo) {
                list($carbonFrom, $carbonTo) = $this->datesToCarbons($fromTo);
                if($carbonFrom->between($startExc, $endExc) && $carbonTo->between($startExc, $endExc)) {
                    unset($aNextOpenHours[$k]);
                }
            }
        }


        /* Station exceptions */
        $aStationExceptions = $this->exceptionTime->toArray();
        foreach($aStationExceptions as $stationException) {
            $startExc = Carbon::parse($stationException['start_datetime']);
            $endExc = Carbon::parse($stationException['end_datetime']);
            foreach($aNextOpenHours as $k => $fromTo) {
                list($carbonFrom, $carbonTo) = $this->datesToCarbons($fromTo);
                if($carbonFrom->between($startExc, $endExc) && $carbonTo->between($startExc, $endExc)) {
                    unset($aNextOpenHours[$k]);
                }
            }
        }

        return $aNextOpenHours;
    }

    /**
     * @param $openCloseDatetimes "2020-07-23 08:00:00 - 2020-07-23 12:00:00"
     * @return array
     */
    private function datesToCarbons(string $openCloseDatetimeDiapason)
    {
        if(!$openCloseDatetimeDiapason) return [[],[]];
        $aOpenClose = explode(' - ', $openCloseDatetimeDiapason);
        $start  = new Carbon($aOpenClose[0]);
        $end    = new Carbon($aOpenClose[1]);
        return [$start, $end];
    }


    /**
     * Initialize next OpenClosed time for ChargingStationController
     * @param $aNextOpenHours
     */
    public function initializeOpenHours($aNextOpenHours)
    {
        if(!$aNextOpenHours) {
            return [];
        }

        foreach($aNextOpenHours as $k => $strOpenClosed){


            if(!$k) {
                // for - status
                self::$isCurrentlyOpen = $strOpenClosed;

                // for - next-time-open
                list($carbonFrom, $carbonTo) = $this->datesToCarbons($strOpenClosed);

                self::$nextStartTimestamp = $carbonFrom->timestamp;
                self::$nextEndTimestamp = $carbonTo->timestamp;
                self::$nextStartDatetime = $carbonFrom->toDateTimeString();
                self::$nextEndDatetime = $carbonTo->toDateTimeString();
            }
        }
    }


}
