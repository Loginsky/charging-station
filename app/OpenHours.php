<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class OpenHours extends Model
{
    use Notifiable;

    // `week_day` field
    const WEEKDAY_MONDAY = 1;
    const WEEKDAY_TUESDAY = 2;
    const WEEKDAY_WEDNESDAY = 3;
    const WEEKDAY_THURSDAY = 4;
    const WEEKDAY_FRIDAY = 5;
    const WEEKDAY_SATURDAY = 6;
    const WEEKDAY_SUNDAY = 7;

    // `user_type` field
    const USER_CUSTOMER = 1;
    const USER_EMPLOYEE = 2;
    const USER_MANAGER = 3;

    protected $table = 'open_hours';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ownable_id', 'ownable_type', 'week_day', 'user_type', 'start_hour', 'end_hour'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_hour' => 'time',
        'end_hour' => 'time',
        'week_day' => 'integer',
        'user_type' => 'integer',
    ];

    /**
     * Get the owning ownable model.
     */
    public function ownable()
    {
        return $this->morphTo();
    }

    /**
     * List of `user_type` names
     *
     * @param null $userType
     * @return array|mixed|null
     */
    public static function userTypeList($userType = null)
    {
        $list = [
            self::USER_CUSTOMER => 'Customer',
            self::USER_EMPLOYEE => 'Employee',
            self::USER_MANAGER => 'Manager',
        ];

        if (!$userType) {
            return $list;
        }

        return array_key_exists($userType, $list) ? $list[$userType] : $userType;
    }
}
