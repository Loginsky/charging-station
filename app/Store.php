<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Tenant;

class Store extends Model
{
    use Notifiable;

    protected $table = 'store';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tenant_id', 'name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
    ];

    /**
     * Get all of the stores's `open_hours`.
     */
    public function openHours()
    {
        return $this->morphMany('App\OpenHours', 'ownable');
    }

    /**
     * Get all of the stores's `exception_time`.
     */
    public function exceptionTime()
    {
        return $this->morphMany('App\ExceptionTime', 'exceptionable');
    }

    public function tenant()
    {
        return $this->belongsTo('App\Tenant', 'tenant_id', 'id');
    }

    public function chargingStation()
    {
        return $this->hasMany('App\ChargingStation','store_id');
    }


}
