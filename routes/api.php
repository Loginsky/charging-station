<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/* return true|false */
Route::get('charging-station/status/{id}', 'API\ChargingStationController@status');

/*
    Db have two seeded and related ids of stations: 3 or 9 for OpenHours timestamp
    Ex.: ../api/charging-station/status/3?datetime=2020-07-21 2018:30:00
*/
Route::get('charging-station/next-time-open/{id}', 'API\ChargingStationController@status');

